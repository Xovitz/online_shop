package net.umbra;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Kamil on 2/9/2017.
 */
public class StorageTest {

    Storage storage;

    @Before
    public void before() {
        storage = new Storage(20, 10);
    }

    @Test
    public void lowerProcessorsNum() throws Exception {
        storage.lowerProcessorsNum(5);
        assertTrue(storage.getProcessorsQty().equals(15));
    }

    @Test
    public void lowerMotherboardsNum() throws Exception {
        storage.lowerMotherboardsNum(8);
        assertTrue(storage.getMotherboardsQty().equals(2));
    }

}