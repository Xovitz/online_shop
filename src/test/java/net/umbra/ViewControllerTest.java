package net.umbra;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

/**
 * Created by Kamil on 2/9/2017.
 */
public class ViewControllerTest {
    Storage storage;
    ViewController viewController;

    @Before
    public void before() {
        storage = new Storage(20, 10);
        viewController = new ViewController(storage);
    }

    @Test(expected = TransactionException.class)
    public void parseNumericInput() throws Throwable {
        Method method = ViewController.class.getDeclaredMethod("parseNumericInput", Commodity.class, String.class);
        method.setAccessible(true);

        try {
            method.invoke(viewController, Commodity.PROCESSOR, "text");
        } catch (InvocationTargetException ex){
            throw ex.getCause();
        }
    }

    @Test
    public void validateNumericInput() throws Exception{
        Method method = ViewController.class.getDeclaredMethod("validateNumericInput", Commodity.class, int.class);
        method.setAccessible(true);
        boolean negativePassed = (boolean)method.invoke(viewController, Commodity.PROCESSOR, -1);
        boolean exceededStorage = (boolean)method.invoke(viewController, Commodity.PROCESSOR, 55);
        assertTrue(!negativePassed && !exceededStorage);
    }

}