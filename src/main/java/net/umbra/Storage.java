package net.umbra;

/**
 * Created by Kamil on 2/8/2017.
 */
public class Storage {
    Integer processorsQty;
    Integer motherboardsQty;

    public Storage(Integer processorsQty, Integer motherboardsQty) {
        this.processorsQty = processorsQty;
        this.motherboardsQty = motherboardsQty;
    }

    public Integer getProcessorsQty() {
        return processorsQty;
    }

    public Integer getMotherboardsQty() {
        return motherboardsQty;
    }

    public void lowerProcessorsNum(int i) {
        processorsQty -= i;
    }

    public void lowerMotherboardsNum(int i) {
        motherboardsQty -= i;
    }
}
