package net.umbra;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;

/**
 * Created by Kamil on 2/7/2017.
 */
@SpringUI
public class MainPage extends UI {

    @Override
    public void init(VaadinRequest request) {
        Storage storage = new Storage(20, 10);
        setContent((new ViewController(storage)).getView());
    }

}
