package net.umbra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CybershopApplication {

    public static void main(String[] args) {
        SpringApplication.run(CybershopApplication.class, args);
        System.out.println("*****************************************" +
                         "\n*                                       *" +
                         "\n*                                       *" +
                         "\n*           localhost:8080              *" +
                         "\n*                                       *" +
                         "\n*                                       *" +
                         "\n*****************************************");

    }
}
