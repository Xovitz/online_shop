package net.umbra;

/**
 * Created by Kamil on 2/8/2017.
 */
public enum Commodity {
    PROCESSOR("processor"), MOTHERBOARD("motherboard");

    String name;

    Commodity(String name) {
        this.name = name;
    }

    public String getName() {return name;}
}
