package net.umbra;

import com.vaadin.server.Page;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;

/**
 * Created by Kamil on 2/8/2017.
 */
public class ViewController {
    private final String PROCESSORS_LABEL = "Processors: ";
    private final String MOTHERBOARDS_LABEL = "Motherboards: ";
    Storage storage;
    Label processorsQty;
    Label motherboardQty;
    TextField processorPurchase = new TextField(PROCESSORS_LABEL);
    TextField motherboardPurchase = new TextField(MOTHERBOARDS_LABEL);

    public ViewController(Storage storage) {
        this.storage = storage;
        processorsQty = new Label(storage.getProcessorsQty().toString());
        motherboardQty = new Label(storage.getMotherboardsQty().toString());
    }

    public Layout getView() {
        final VerticalLayout layout = new VerticalLayout();
        HorizontalLayout itemAQtyLayout = new HorizontalLayout(new Label(PROCESSORS_LABEL), processorsQty);
        HorizontalLayout itemBQtyLayout = new HorizontalLayout(new Label(MOTHERBOARDS_LABEL), motherboardQty);

        layout.addComponents(new Label("<b>Currently available in our shop:</b>", ContentMode.HTML),
                            itemAQtyLayout,
                            itemBQtyLayout,
                            new Label("&nbsp;", Label.CONTENT_XHTML),
                            new Label("<b>Your shopping:</b>", ContentMode.HTML),
                            processorPurchase,
                            motherboardPurchase,
                            getBuyButton());
        layout.setSpacing(true);
        itemAQtyLayout.setMargin(new MarginInfo(false, true, false, true));
        itemBQtyLayout.setMargin(new MarginInfo(false, true, false, true));
        layout.setMargin(new MarginInfo(true, true, false, true));
        return layout;
    }

    private Button getBuyButton() {
        return new Button("Buy", clickEvent -> {
            int procOrderedQty;
            int mothOrderedQty;
            try {
                procOrderedQty = parseNumericInput(Commodity.PROCESSOR, processorPurchase.getValue());
                mothOrderedQty = parseNumericInput(Commodity.MOTHERBOARD, motherboardPurchase.getValue());
            } catch (TransactionException ex) {
                return;
            }

            if (procOrderedQty != 0) {
                storage.lowerProcessorsNum(procOrderedQty);
                processorsQty.setValue(storage.getProcessorsQty().toString());
            }
            if (mothOrderedQty != 0) {
                storage.lowerMotherboardsNum(mothOrderedQty);
                motherboardQty.setValue(storage.getMotherboardsQty().toString());
            }
            resetInputs();
        });
    }

    private void resetInputs() {
        processorPurchase.setValue("");
        motherboardPurchase.setValue("");
    }

    private int parseNumericInput(Commodity commodity, String numInput) throws TransactionException {
        numInput = numInput.trim();
        int number;
        try {
            number = numInput.isEmpty() ? 0 : Integer.parseInt(numInput);
        } catch (NumberFormatException ex) {
            showWarning("Invalid number in " + commodity.getName() + " input.");
            throw new TransactionException();
        }
        if (!validateNumericInput(commodity, number)) throw new TransactionException();
        return number;

    }

    private boolean validateNumericInput(Commodity commodity, int number) {
        if (number < 0) {
            showWarning("Asked number of " + commodity.getName() + "s, can't be negative.");
            return false;
        } else if (commodity == Commodity.PROCESSOR && number > storage.getProcessorsQty()) {
            showWarning("Asked number of processors is too big.");
            return false;
        } else if (commodity == Commodity.MOTHERBOARD && number > storage.getMotherboardsQty()) {
            showWarning("Asked number of motherboards is too big.");
            return false;
        }
        return true;
    }

    private void showWarning(String warning) {
        if(Page.getCurrent() == null) return;
        new Notification("Warning",
                "<br/>" + warning,
                Notification.Type.WARNING_MESSAGE, true)
                .show(Page.getCurrent());
    }
}
